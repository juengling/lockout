'''
Created on 11.02.2019

@author: chris
'''
from argparse import ArgumentParser
from collections import Counter
from os.path import expanduser
import os
import sys

__title__ = 'lockout'


def main(argv):
    '''
    Lockout analyses emails with lockout messages saved to files (*.eml)
    and prints a list of usernames and IP ranges with occurance counts.
    '''
    args = parse_command_line(argv)

    usernames = Counter()

    for filename in os.listdir(expanduser(args.folder)):
        if filename.endswith(".eml"):
            fullpath = expanduser(os.path.join(args.folder, filename))
            with open(fullpath) as file:
                for line in file:
                    if line.startswith('Username: '):
                        username = line[10:].rstrip()
                        usernames [username] += 1

    for username, count in usernames.most_common():
        print('{}: {}'.format(username, count))


def parse_command_line(arguments):
    '''
    Parse the command line arguments

    :param arguments: List of command line arguments
    :return: Arguments object
    '''
    parser = ArgumentParser(prog = __title__, description = main.__doc__)

    parser.add_argument('folder',
                        help = 'Name/path of the folder with .eml files')

    args = parser.parse_args(arguments)
    return args


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))

